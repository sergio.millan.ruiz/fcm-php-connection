# FCM PHP connection

Ejemplo de clase desarrollada para un WordPress con Woocommerce que envía notificaciones a los dispositivos que tenga registrados el usuario cuando un pedido cambia de estado a "completado" o "en espera".

## Aclaraciones
* Para enviar notificaciones es necesario realizar el registro del dispositivo en FCM. Eso se está haciendo desde una app móvil desarrollada en Ionic, en este caso con el (plugin de Phonegap)[https://ionicframework.com/docs/native/push].
* También hay que guardar en back el Unique Device ID (se está obtiendo desde la app también, utilizando el plugin (Unique Device ID)[https://ionicframework.com/docs/native/unique-device-id]) del dispositivo junto con el token que devuelve FCM al registrar el dispositivo (se actualiza cada poco tiempo) para poder mandarle las notificaciones.
* En este ejemplo, la clave del proyecto FCM se está guardando en un option de WordPress, que en caso de no estar relleno se muestra una notificación en el panel de administración.
* Hay varios métodos creados para consultar la información de FCM y poder interactuar para enviar las notificaciones.
* Como en este caso se tiene instalado Woocommerce, se ha hecho una prueba para enviar notificaciones cuando los pedidos cambian de estado.
* **ES IMPORTANTE** observar cómo está compuesta la estructura de la notificación, ya que no viene así en la documentación pero es la forma en la que funciona la recepción de la data de la notificación tanto cuando la app esta en foreground, como en background.